======
Linker
======

The task of the linker is to combine object code into an executable.
Object code typically comes from two sources:

-  own code compiled from source
-  external libraries


Static Linking
==============
Static linking means that the whole code required for running the application is stored in a single file.
This can be illustrated based on the files from the previous section.

.. code-block:: sh

   gfortran HelloWorld.o Hello.o -o hello_Fortran
   ./hello_Fortran

Note that the object code is *language independent*.
Hence, nothing prevents us from linking the C main program with the Fortran function

.. code-block:: sh

   gfortran hello_world.o Hello.o -o hello_C
   ./hello_C


To see what the linker command actually means, run the command with the ``-###`` option.

.. code-block:: sh

   gfortran hello_world.o Hello.o -o hello_C -###

As seen at the end, ``gfortran`` is delegating the linker step to ``collect2`` and calls it with about 20 options.
``gcc`` would also call ``collect2`` but with different options.
The options added by ``gfortran`` are required to link to the Fortran library, that is why

.. code-block:: sh

   gcc hello_world.o Hello.o -o hello_C

does not work.

.. note::

   The ``-###`` option also works for the compile step (``gfortran -c`` and ``gcc -c``).


Dynamic Linking
===============

We have seen that linking literally links the content object code files into one file.
This approach, called `static linking` has two major drawbacks

- Waste of memory
- Difficult update procedure

Memory waste results from the fact that parts of many libraries (think of the C standard library) are used by multiple programs.
Moreover, statically linked programs need to be updated every time that one of its libraries is updated.

To avoid these problems, dynamic linking is used.
That means that libraries are stored centrally and can be loaded by the program during execution.
The extension of dynamically libraries is ``so`` (shared object, unix-like), ``dll`` (dynamic-link library, Windows), or ``dynlib`` (MacOS).

The following creates a shared object with position-independent code (``-fPIC``) code.

.. code-block:: shell

   gfortran -shared -fPIC Hello.f90 -o libhello.so

The file follows the common naming convention leading ``lib``, only small letters, ``so`` extension.

.. note::
   
   It is common to append the version of the library to shared objects.
   A library called ``example`` in version ``1.2.3`` would be stored in a file called ``libexample.so.1.2.3``.
   To make it possible to link the library by its canonical name, symbolic links named ``libexample.so`` and ``libexample.so.1`` are usually provided in addition.

The following command builds the application:

.. code-block:: shell

   gfortran -L$PWD HelloWorld.f90 -lhello -o hello_Fortran

The option `-L` gives the search path (here the current directory), `-l` tells it to link a file ``libhello.so`` that needs to be available in its search path.
These options only ensure that the library exists during compile-time, it is however also needed during run-time.

For that reason, the following command

.. code-block:: shell

   ./hello_Fortran

fails with ``./hello_Fortran: error while loading shared libraries: libhello.so: cannot open shared object file: No such file or directory``

To inspect the reason, `ldd` can be used

.. code-block:: shell

   ldd hello_Fortran

which reveals that `libhello.so` is not found.
Obviously, the current path is not searched for libraries.

There are (at least) two different ways to fix this issue:

1. Add the current directory to the *search path of the executable*:

.. code-block:: shell

   gfortran -L$PWD -Wl,-rpath,$PWD HelloWorld.f90 -lhello -o hello_Fortran

.. note::

  ``-Wloption`` passes ``option`` as an option to the linker.
  If ``option`` contains commas, it is split into multiple arguments at the commas.
  Here, ``-rpath $PWD`` is passed to the linker.

2. Add the current directory to the *global search path*:

.. code-block:: shell

   export LD_LIBRARY_PATH=$PWD:$LD_LIBRARY_PATH

Normally, shared objects are located in a standard location like `/usr/lib`.
In that case, neither using `Wl,-rpath` nor setting `LD_LIBRARY_PATH` is needed.

Inspecting with ``ldd`` reveals no issues and executing the program is now possible:

.. code-block:: sh

   ldd hello_Fortran
   ./hello_Fortran

Additional Information
======================
- https://www.cprogramming.com/tutorial/shared-libraries-linux-gcc.html
- https://gcc.gnu.org/onlinedocs/gccint/Collect2.html
- https://www.delorie.com/djgpp/doc/ug/basics/compiling.html
