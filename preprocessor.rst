============
Preprocessor
============

As the name implies, a preprocessor is invoked before the actual compilation.
Preprocessing source code can be used to overcome limitations of the actual language.
While C and C++ have their 'own' preprocessor called `cpp`, preprocessing is less common for Fortran.
Typically, the C preprocessor in traditional mode is used for Fortran.
Preprocessor statements start with a `#` in the *first column*.

Invoking
========
The preprocessor is typically involved trough the compiler driver, e.g. gcc or gfortran.
By convention, Fortran source files with upper case extension (`*.F90`) are preprocessed, files with lower case extension not.
This behavior can be changed by explicitly requestion preprocessing with `-cpp`.

Variables
=========
Variable can be defined either in the code or via the `-D` option

.. code-block:: C

   # test.C
   int main() {
   # define v 3
     int example = v;
     hello_(&example);
     example = w
     hello_(&example);
     return 0;
   }

.. code-block:: sh

   cpp test.C -Dw=4 > test.c
   cat test.c

.. code-block:: Fortran

   ! test.F90
   program HelloWorld
   #define v 5
     implicit none
     call Hello(v)
     call Hello(w)
   end program HelloWorld

.. code-block:: sh

   cpp -lang-Fortran test.F90 -Dw=6 > test.f90
   cat test.f90

.. code-block:: Fortran

   ! test2.F90
   program HelloWorld
   #define v 7
     implicit none
     print*, v
     print*, w
   end program HelloWorld

.. code-block:: sh

   gfortran test2.F90 -o test2
   ./test2

Include
=======

An include statement, ``#include`` includes the content of another file.
It is commonly used in C to include headers


Control Flow
============
The commands for branching are ``#if``, ``#elif``, ``#else`` and ``#endif``.
Conditional compilation can be done by checking the existence of a variable (``#ifdef`` or ``#defined()``) or comparing their values (``>``, ``>=``, ``==``, ``<=``, ``<=``).


Additional Information
======================
- https://software.intel.com/content/www/us/en/develop/documentation/fortran-compiler-oneapi-dev-guide-and-reference/top/compiler-reference/compiler-options/compiler-option-details/preprocessor-options/fpp.html
- https://fypp.readthedocs.io
- https://gcc.gnu.org/onlinedocs/cpp/Include-Syntax.html
- http://gcc.gnu.org/onlinedocs/cpp/index.html#Top
